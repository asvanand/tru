from pyspark import SparkContext
from pyspark.sql import SQLContext
from pyspark.sql import functions as F
from pyspark.sql import SparkSession
import os


def expand_rows(row):
    for k in row:
        yield k


def rename_file(filepath, task_no):
    # renames automated file name to tailor made name
    spark = SparkSession.builder.getOrCreate()
    from py4j.java_gateway import java_import
    java_import(spark._jvm, 'org.apache.hadoop.fs.Path')
    fs = spark._jvm.org.apache.hadoop.fs.FileSystem.get(spark._jsc.hadoopConfiguration())

    # Get filename, rename the file & then delete temp file
    file_name = fs.globStatus(sc._jvm.Path(filepath + "/temp/part*"))[0].getPath().getName()
    fs.rename(sc._jvm.Path(f"{filepath}/temp/{file_name}"), sc._jvm.Path(f"{filepath}/out_2_{task_no}.txt"))
    fs.delete(sc._jvm.Path(filepath + "/temp/"), True)


def chore_task(df, task_no):
    # Execute tasks

    # Part2 - Task
    if task_no == 2:
        out_df = df.agg(F.min(df.price).alias("min_price"), F.max(df.price).alias("max_price"),
                        F.count(F.lit(1)).alias("row_count"))

    # Part2 - Task2
    elif task_no == 3:
        out_df = df.filter((df['price'] > 5000) & (df['review_scores_rating'] == 10)).agg(
            F.avg(df.bathrooms).alias("avg_bathrooms"), F.avg(df.bedrooms).alias("avg_bedrooms"))

    # Part2 - Task4
    elif task_no == 4:
        out_df = df.select(F.struct('accommodates', 'price', 'review_scores_rating').alias("TA")).select(
            F.min("TA.price"), F.max("TA.review_scores_rating"),
            F.first("TA.accommodates").alias("accommodates")).select("accommodates")

    else:
        print("Error: Invalid task")
        raise

    # Write out the file
    try:

        out_df.coalesce(1).write.format('csv').mode("Overwrite").option('header', True).option('sep', ',').save(
            f"{out_folder}/temp/")

        rename_file(out_folder, task_no)

    except Exception as e:
        output = f"{e}"
        return_code = 'FAIL'
        print(return_code, output)
        raise


if __name__ == "__main__":

    # Python 3.9
    os.environ["PYSPARK_PYTHON"] = "python3.9"
    os.environ["PYSPARK_DRIVER_PYTHON"] = "python3.9"

    # global ip_folder, out_folder
    ip_folder = "truata/in"
    out_folder = "truata/out/"

    sc = SparkContext()
    sqlContext = SQLContext(sc)

    # Part1 - Task 1
    # get unique grocery product and count
    rdd = sc.textFile(f"{ip_folder}/groceries.csv").map(lambda line: line.split(",")).map(
        lambda s: s).flatMap(expand_rows).distinct().collect()

    # write into temp folder
    sc.parallelize(rdd).coalesce(1).map(lambda row: str(row)).saveAsTextFile(f"{out_folder}/temp/")

    # rename the file
    rename_file(out_folder, "2a")

    ### Part 1 ###
    # Part1 - Task 2
    # total products count
    sc.parallelize(rdd).coalesce(1).map(lambda row: str(row)).saveAsTextFile(f"{out_folder}/temp/")
    rdd = len(sc.textFile("truata/in/groceries.csv").map(lambda line: line.split(",")).map(
        lambda s: s).flatMap(expand_rows).collect())
    rename_file(out_folder, "2b")

    ### Part 2 ###
    # Task 1 - Read parquet using dataframe
    df = sqlContext.read.parquet(
        f"{ip_folder}/part-00000-tid-4320459746949313749-5c3d407c-c844-4016-97ad-2edec446aa62-6688-1-c000.snappy.parquet")

    # for Tasks 2,3,4
    for i in range(3):

        chore_task(df, i + 2)
